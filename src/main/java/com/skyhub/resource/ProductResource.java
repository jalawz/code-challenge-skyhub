package com.skyhub.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.skyhub.event.CreatedResourceEvent;
import com.skyhub.model.Product;
import com.skyhub.repository.ProductRepository;
import com.skyhub.service.ProductService;

@RestController
@RequestMapping("/products")
public class ProductResource {
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@GetMapping
	public ResponseEntity<List<Product>> listProducts() {
		List<Product> productList = productRepository.findAll();
		return ResponseEntity.ok(productList);
	}
	
	@PostMapping
	public ResponseEntity<Product> registerProduct(@RequestBody Product product, HttpServletResponse response) {
		Product productSaved = productRepository.save(product);
		publisher.publishEvent(new CreatedResourceEvent(this, response, productSaved.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(productSaved);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Product> getProduct(@PathVariable Long id) {
		Product product = productRepository.findById(id).orElse(null);
		return product != null ? ResponseEntity.ok(product) : ResponseEntity.notFound().build();
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Object> updateProduct(@PathVariable Long id, @RequestBody Product product) {
		productService.updateProduct(id, product);
		return ResponseEntity.noContent().build();
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteProduct(@PathVariable Long id) {
		productService.deleteProduct(id);
	}
}
