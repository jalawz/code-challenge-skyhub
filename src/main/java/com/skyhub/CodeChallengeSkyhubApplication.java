package com.skyhub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CodeChallengeSkyhubApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodeChallengeSkyhubApplication.class, args);
	}

}
