package com.skyhub.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.skyhub.model.Product;
import com.skyhub.repository.ProductRepository;

@Service
public class ProductService {
	
	@Autowired
	private ProductRepository productRepository;
	
	public Product updateProduct(Long id, Product product) {
		Product savedProduct = findProductById(id);
		BeanUtils.copyProperties(product, savedProduct, "id");
		return productRepository.save(savedProduct);
	}
	
	public void deleteProduct(Long id) {
		if (findProductById(id) == null) {
			throw new EmptyResultDataAccessException(1);
		}
		productRepository.deleteById(id);
	}
	
	public Product findProductById(Long id) {
		Product product = productRepository.findById(id).orElse(null);
		if (product == null) {
			throw new EmptyResultDataAccessException(1);
		}
		
		return product;
	}
}
