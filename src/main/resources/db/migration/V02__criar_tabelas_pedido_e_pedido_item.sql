CREATE TABLE orders (
	id BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL,
	order_date DATE NOT NULL,
	buyer_name VARCHAR(50) NOT NULL,
	status VARCHAR(50) NOT NULL,
	freight_value DECIMAL(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE order_item (
	order_id BIGINT(20) NOT NULL,
	product_id BIGINT(20) NOT NULL,
	quantity BIGINT(20) NOT NULL,
	FOREIGN KEY (order_id) REFERENCES orders(id),
	FOREIGN KEY (product_id) REFERENCES product(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO orders (order_date, buyer_name, status, freight_value) VALUES ('2018-05-30', 'Mazaroppi', 'APROVADO', 40);
INSERT INTO order_item(order_id, product_id, quantity) VALUES (1, 1, 1);
INSERT INTO orders (order_date, buyer_name, status, freight_value) VALUES ('2019-05-22', 'Tony Stark', 'ENTREGUE', 25);
INSERT INTO order_item(order_id, product_id, quantity) VALUES (2, 1, 4);
INSERT INTO order_item(order_id, product_id, quantity) VALUES (2, 2, 10);


