CREATE TABLE product (
	id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(100) NOT NULL,
	description VARCHAR(100) NOT NULL,
	stock_quantity BIGINT(20) NOT NULL,
	price DECIMAL(10, 2) NOT NULL,
	attributes VARCHAR(100)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO product (name, description, stock_quantity, price, attributes) VALUES ('iPhone XR 64gb', 'Apple iPhone XR de 64gb de memória', 100, 3500, 'preto, tela retina, camera 22mpx');
INSERT INTO product (name, description, stock_quantity, price, attributes) VALUES ('Xiaomi Pocophone F1', 'Celular com 128gb de memória', 100, 1750, 'cinza, tela full hd+, camera 22mpx');
INSERT INTO product (name, description, stock_quantity, price, attributes) VALUES ('Smart TV LG', 'Smart Tv LG com apps', 50, 2200, 'prata, 4k, smartv');
INSERT INTO product (name, description, stock_quantity, price, attributes) VALUES ('Mouse Logitech Wireless', 'Mouse sem fio a pilha', 200, 59, 'preto, sem fio, moderno');
INSERT INTO product (name, description, stock_quantity, price, attributes) VALUES ('Notebook Lenovo i5', 'Notebook processador i5 da lenovo', 50, 2500, 'Cinza, tela 15.6, teclado retro-iluminado');
INSERT INTO product (name, description, stock_quantity, price, attributes) VALUES ('Impressora Epson', 'Impressora multifuncional epson', 75, 350, 'preto, cartucho colorido, cartucho preto, wireless');